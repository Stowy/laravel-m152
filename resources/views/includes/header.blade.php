<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">{{config('app.name')}}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item @if (Request::is('/')) active @endif">
                <a class="nav-link" href="{{ route("index") }}">Home</a>
                </li>
                <li class="nav-item @if (Route::current()->getName() == 'posts.create') active @endif">
                    <a class="nav-link" href="{{ route("posts.create") }}">Post</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
