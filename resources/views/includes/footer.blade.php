<!-- Footer -->
<footer class="bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Fabian Huber - CFPT 2019</p>
    </div>
    <!-- /.container -->
</footer>
