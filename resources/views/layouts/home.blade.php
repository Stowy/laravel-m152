@extends('layouts.default')

@section('content')
<div class="row">
    <!-- Blog Entries Column -->
    <div class="col-md-8">

        <h1 class="my-4">
            My Blog
            <small>It's very the blog</small>
        </h1>

        @yield('posts')
    </div>

    <!-- Sidebar Widgets Column -->
    <div class="col-md-4">
        @include('includes.sidewidgets.categories')
    </div>

</div>
<!-- /.row -->
@endsection
