<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.head')
</head>

<body>
    @include('includes.header')

    <!-- Page Content -->
    <div class="container">
        <div id="main" class="container clear-top">
            @yield('content')
        </div>
    </div>
    <!-- /.container -->

    @include('includes.footer')

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
