@extends('layouts.home')

@section('title', 'Home')


@if(isset($post))
    @foreach ($posts as $post)
        <!-- Blog Post -->
        <div class="card mb-4">
            <img class="card-img-top" src="/storage/cover_images/{{ $post->media }}" alt="Card image cap">
            <div class="card-body">
                <p class="card-text">{{ $post->comment }}</p>
            </div>
            <div class="card-footer text-muted">
                Posted on {{ $post->created_at }}
            </div>
        </div>
    @endforeach
    {{ $posts->links() }}
@else
@section('posts')
    <!-- Blog Post -->
    <div class="card mb-4">
        <img class="card-img-top" src="https://picsum.photos/seed/hackerman/750/300" alt="Card image cap">
        <div class="card-body">
            <p class="card-text">Lorem ipsum dolor sit amet, Florida Man Attacked by Python Hiding in Toilet</p>
        </div>
        <div class="card-footer text-muted">
            Posted on January 1, 2017
        </div>
    </div>
@endif
@endsection
