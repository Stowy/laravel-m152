@extends('layouts.default')

@section('title', 'Post')

@section('content')
<h1>Create Post</h1>
<form action="{{route('posts.store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <input type="file" name="media">
    </div>
    <div class="form-group">
        <label for="comment">Comment</label>
        <input type="text" name="comment" id="form-control" placeholder="Comment">
    </div><br>
    <input type="submit" class="btn btn-primary">
</form>
@endsection
